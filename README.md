# Kotlin Workshop

Repository with tasks for Kotlin workshops.
Simply `clone` or fork this repository and wait for lecturer to guide you through :-).

You may want to copy whole project into your own private namespace or different repository to keep
track of your personal changes.
