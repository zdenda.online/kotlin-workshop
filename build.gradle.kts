import org.gradle.api.tasks.testing.logging.TestExceptionFormat
import org.jetbrains.kotlin.gradle.dsl.jvm.JvmTargetValidationMode
import org.jetbrains.kotlin.gradle.tasks.KotlinJvmCompile

plugins {
    kotlin("jvm") version "1.9.24"
    java
}

group = "online.zdenda"
version = "1.0"

repositories {
    mavenCentral()
}

tasks.withType<KotlinJvmCompile>().configureEach {
    jvmTargetValidationMode.set(JvmTargetValidationMode.WARNING)
}

tasks {
    test {
        useJUnitPlatform {
            testLogging.exceptionFormat = TestExceptionFormat.FULL
            testLogging.showStandardStreams = true
        }
    }
}

kotlin {
    jvmToolchain(17)
}

dependencies {
    val koTestVersion: String by project
    testImplementation("io.kotest:kotest-runner-junit5-jvm:$koTestVersion")
    testImplementation("io.kotest:kotest-framework-datatest-jvm:$koTestVersion")

    val assertjVersion: String by project
    testImplementation("org.assertj:assertj-core:$assertjVersion")
}


