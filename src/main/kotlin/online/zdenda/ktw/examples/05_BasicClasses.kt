package online.zdenda.ktw.examples

class Developer(val name: String, var age: Int) { // primary constructor

    var upperName: String = name.uppercase()

    fun birthday() {
        age++
    }
}

data class DataDeveloper(val name: String, var age: Int) // making fields (get/set) from primary constructor right away
// similar to Java record but
// - classes may have non-final (var) properties
// - can have instance variables (defined within body of class)
// - have .copy feature
// - no base class (Java records extend Record class) and thus can extend other classes

fun main() {
    val talentedDev = Developer("John Doe", 15) // there is no new keyword!
    talentedDev.birthday()
    println("${talentedDev.upperName} is now ${talentedDev.age}")


    val devA = Developer("X", 10)
    val devB = Developer("X", 10)
    println(devA == devB) // not a data class will result in false
    println(devA) // regular Java's toString == reference

    val devC = DataDeveloper("X", 10)
    val devD = DataDeveloper("X", 10)
    println(devC == devD) // data class has equals, hashCode, toString...
    println(devC) // pretty toString including properties

    val devE = devC.copy(age = 12)
    println(devE) // copy, keep original
    val (name, age) = devE // "destructing" declaration from data class
    println("$name is $age years old")
}
