package online.zdenda.ktw.examples

// notice no semicolons

val greatQuestionAnswer = 42 // type inference to Int and "final"
const val GREAT_QUESTION_ANSWER = 42 // const are known at compile-time
const val GREAT_QUESTION_ANSWER_TYPED: Int = 42 // type can be defined explicitly, name: type syntax

fun showVal() {
    println("Answer to the Ultimate Question of Life, The Universe, and Everything is $greatQuestionAnswer")
    println("Typed answer is also $GREAT_QUESTION_ANSWER and $GREAT_QUESTION_ANSWER_TYPED")
}

var year = 2023

fun showVar() {
    year += 1 // or year = year + 1
    println("Next year will be $year, the year after will be ${year + 1}")
}

fun main() {
    showVal()
    showVar()
}