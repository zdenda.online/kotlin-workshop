package online.zdenda.ktw.examples

fun main() {

    val list: List<Int> = listOf(1, 2, 3, 4, 5)
    // cannot add values as it is immutable!

    val mutableList = mutableListOf(1, 2, 3, 4, 5)
    mutableList.add(4)

    val mutableListConverted = list.toMutableList()
    val andBack = mutableListConverted.toList() // now immutable again

    val finalList = list.filter { e -> e % 2 == 0 } // no need to convert to .stream() and back via .collect()
    println(finalList)

    val filterFunction: (e: Int) -> Boolean = { e -> e % 2 == 0 }
    val anotherFinalList = list.filter(filterFunction)
    println(anotherFinalList)

    val maxDoubled = list.filter { it > 1 } // use 'it' as default element
        .map { it * 2 }
        .maxOrNull()
    println(maxDoubled)

    val mapInline = mapOf( // inline definition of map entries
        1 to "one",
        2 to "two",
        3 to "three"
    )
    println(mapInline)
    for (entry in mapInline) { // iterate through entries the basic way
        println("${entry.key} is ${entry.value}")
    }
    for ((key, value) in mapInline) { // or use destructing for key and value
        println("$key is $value")
    }
}
