package online.zdenda.ktw.examples

import java.io.File

interface Programmer {
    fun codeSomeStuffMakingMoney()
}

class JuniorProgrammer : Programmer {

    override fun codeSomeStuffMakingMoney() { // override is keyword, not an annotation
        println("println(\"Hello world!\")")
    }
}

// class SeniorProgrammer: JuniorProgrammer() // not allowed as it is not open

open class RegularProgrammer(private val helloWhat: String) : Programmer { // private field constructor

    override fun codeSomeStuffMakingMoney() {
        println("println(\"Hello $helloWhat\")")
    }
}

class SeniorProgrammer : RegularProgrammer("big world") {

    fun codeSomethingUseful() {
        println("val a = 1 + 1")
    }
}

sealed interface Error
sealed class IOError : Error
class FileError(val file: File) : IOError()
data object NetworkError : IOError()
class RuntimeError(val message: String) : Error

fun main() {
    val programmer = JuniorProgrammer()
    programmer.codeSomeStuffMakingMoney()

    val senior = SeniorProgrammer()
    senior.codeSomeStuffMakingMoney()
    senior.codeSomethingUseful()

    val error: Error = FileError(File("."))
    when (error) { // sealed classes are some sort of "enums"
        is FileError -> println("file error of file in path: '${error.file}'") // smart cast!
        is NetworkError -> println("network error!")
        is RuntimeError -> println("we are doomed: ${error.message}")
    }
}
