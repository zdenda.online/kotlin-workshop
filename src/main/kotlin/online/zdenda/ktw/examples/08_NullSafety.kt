package online.zdenda.ktw.examples

fun main() {
    // val x: Int = null // not allowed
    var nullableInt: Int? = null
    nullableInt = 42 // can assign a value

    val fineInt: Int = nullableInt // smart cast, it knows it is not null

    val otherNullableInt: Int? = null
    if (otherNullableInt != null) {
        println(otherNullableInt.inc()) // smart cast, it knows it is not null now
    }

    println(playWithNullability(null))

    val nullableCollection: List<Int?> = listOf(1, 2, null, 10)
}

fun playWithNullability(nullableInt: Int?) {
    println(nullableInt?.dec()) // safe call
    println(nullableInt ?: 0) // elvis
    println(nullableInt?.dec() ?: 0) // combine safe call with elvis
    println(nullableInt.toString()) // :thinking_face: how come if it is null? (nullable receiver!)
    val fineInt: Int = nullableInt!! // now it cannot know, but I can force that I know (dangerous though!!)

    // Note that it can happen in rare cases to have null in non-nullable variable if "put" there from outside context
    // e.g. during deserialization via external library that uses reflection = then if (x == null) can "make sense"
}