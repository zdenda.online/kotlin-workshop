val int: Int = 1000 // or Byte, Short, Long...
val string: String = "yep, I'm string"
val double: Double = 3.14 // or Float
val long: Long = 100_000_000_000_000 // can use underscores for better readability
val boolean: Boolean = true // stay positive
val character: Char = 'x'

// note that all data types are classes!
