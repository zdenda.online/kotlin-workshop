package online.zdenda.ktw.examples

fun answer(question: String): String {
    return "42"
}

fun add(a: Int, b: Int): Int = (a + b) // can be defined inline

fun divide(a: Int, b: Int = 1) = (a / b)

fun isNice(isPretty: Boolean, isAmazing: Boolean): Boolean = (isPretty || isAmazing)

val customIncrement: (input: Int) -> Int = { input -> input + 1 } // function type

fun main() {
    val answer = answer("Ultimate Question of Life, The Universe, and Everything")
    println(answer)

    println(add(1, 2))
    println(divide(10, 5))
    println(divide(10)) // second argument has default value
    println(isNice(isPretty = true, isAmazing = false)) // named arguments, especially useful for booleans
    println(customIncrement(42))
}