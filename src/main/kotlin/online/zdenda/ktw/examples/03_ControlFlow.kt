package online.zdenda.ktw.examples

enum class Grade { A, B, C, D, E }

fun main() {
    val answer = 42

    if (answer == 42) { // this is equals actually, for reference equality use ===
        println("yes")
    } else {
        println("oh no!")
    }

    // there is no ternary operator, but you can inline 'if' with the same effect
    println(if (answer == 42) "yes again" else "oh no again") // if is an expression

    when (answer) {
        42 -> println("yes from when") // no break; needed here
        else -> println("no")
    }

    when { // can be without "variable"m just with expressions
        (answer == 42) -> println("yes from when without variable")
        (System.currentTimeMillis() > 0) -> println("Only first matching is evaluated")
        else -> println("no")
    }

    val grade = Grade.entries.random()
    val parentsReaction = when (grade) { // can assign result to variable directly
        Grade.A -> "great"
        Grade.B -> "kinda ok"
        Grade.C -> 10 // parentsReaction was String, now it is Comparable && Serializable only
        Grade.D -> "dude wtf"
        Grade.E -> "get out" // checks whether all enum values are present
        else -> "" // not necessary as all branches are present
    }
    println("You got $grade? $parentsReaction")

    for (i in 1..10) { // this range INCLUDES last value
        print("$i ")
    }
    println()
    for (i in 1..<10) { // this range EXCLUDES last value
        print("$i ")
    }
    println()

    val arrayOfInts = 1..10 step 2 // 1, 3, 5, 7, 9
    for (anInt in arrayOfInts) {
        print("$anInt ")
    }
    println()

    var iterations = 0
    do {
        iterations++
    } while (Math.random() > 0.5)
    println("Made $iterations iterations")
}