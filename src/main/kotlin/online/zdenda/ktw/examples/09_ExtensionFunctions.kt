package online.zdenda.ktw.examples

fun Int.addTwo(): Int { // solves UtilityClass in some ways
    return this + 2
}

fun main() {
    println(5.addTwo())
}
