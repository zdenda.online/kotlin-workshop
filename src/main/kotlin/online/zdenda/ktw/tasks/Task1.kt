package online.zdenda.ktw.tasks

import online.zdenda.ktw.tasks.series.Series

/**
 * Task #1 is related to basic operations within collections.
 *
 * Goal is to provide names of characters from given series that contain letter `Y` (case-insensitive) in the name.
 * Output names should be sorted naturally (by alphabet) so that tests are stable.
 *
 * Hints:
 * - there is no need to transform collection into [java.util.stream.Stream] and back via [java.util.stream.Collector]
 * - it is not necessary to lower-case two compared values before using [contains]
 * - there are built-in functions how to sort any collection naturally in a functional way
 */
class Task1 : Task<Series, List<String>> {

    override fun solve(input: Series): List<String> {
        TODO("Not yet implemented")
    }
}