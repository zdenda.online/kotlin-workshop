package online.zdenda.ktw.tasks.series

/**
 * Represents a character with hobbies :-)
 */
data class Character(

    /**
     * Name of the character, may be full or short.
     */
    val name: String,
    /**
     * Most favourite hobbies of the character.
     */
    val hobbies: List<String>
)