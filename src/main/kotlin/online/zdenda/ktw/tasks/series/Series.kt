package online.zdenda.ktw.tasks.series

/**
 * Represents a series aired in TV or elsewhere.
 */
data class Series(

    /**
     * Official name of the series.
     */
    val name: String,
    /**
     * Characters that appear in the series.
     */
    val characters: List<Character>
)