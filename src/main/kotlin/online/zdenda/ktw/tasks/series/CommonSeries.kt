package online.zdenda.ktw.tasks.series

val friendsSeries = Series(
    "Friends", listOf(
        Character("Monica Geller", listOf("cleaning", "cooking", "dancing")),
        Character("Rachel Green", listOf("fashion")),
        Character("Phoebe Buffay", listOf("music", "massage")),
        Character("Chandler Bing", listOf("jokes", "data")),
        Character("Ross Geller", listOf("archeology", "dancing")),
        Character("Joey Tribbiani", listOf("food", "acting", "dating"))
    )
)

val himymSeries = Series(
    "How I Met Your Mother", listOf(
        Character("Lily Aldrin", listOf("art", "shopping", "kids")),
        Character("Robin Scherbatsky", listOf("guns", "sport", "acting", "music")),
        Character("Ted Mosby", listOf("architecture", "crosswords")),
        Character("Marshall Eriksen", listOf("law", "music", "food", "comics")),
        Character("Barney Stinson", listOf("fashion", "dating"))
    )
)

val tbbtSeries = Series(
    "The Big Bang Theory", listOf(
        Character("Penny", listOf("fashion", "acting", "dating", "sport")),
        Character("Leonard Hofstadter", listOf("comics", "games", "physics")),
        Character("Sheldon Cooper", listOf("comics", "games", "physics")),
        Character("Howard Wolowitz", listOf("comics", "games", "engineering", "fashion")),
        Character("Raj Koothrappali", listOf("comics", "games", "astrology", "dating"))
    )
)

val emptySeries = Series(
    "Empty", emptyList()
)

val commonKnownSeries = listOf(friendsSeries, himymSeries, tbbtSeries)