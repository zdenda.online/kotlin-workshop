package online.zdenda.ktw.tasks

/**
 * Task that is worth solving!
 */
interface Task<in I, out O> {

    /**
     * Solves the task providing desired output from given input.
     */
    fun solve(input: I): O
}