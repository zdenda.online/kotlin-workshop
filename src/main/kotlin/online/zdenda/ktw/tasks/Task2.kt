package online.zdenda.ktw.tasks

import online.zdenda.ktw.tasks.series.Series

/**
 * Task #2 is related to advanced operations within collections along with simple nullability checks.
 *
 * Goal is to find the most frequent hobby of all characters from provided series.
 * Output should be single (first found) most frequent hobby or empty string in case there are no hobbies at all.
 *
 * Hints:
 * - there is an easy way how to flatten list of lists into single list (through single method call)
 * - consider creation of [Grouping] as it has some useful methods for counting occurrences
 * - [maxBy] method is deprecated since Kotlin 1.4 in favor of other function
 */
class Task2 : Task<List<Series>, String> {

    override fun solve(input: List<Series>): String {
        TODO("Not yet implemented")
    }
}