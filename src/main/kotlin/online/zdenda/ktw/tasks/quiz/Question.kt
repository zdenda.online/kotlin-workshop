package online.zdenda.ktw.tasks.quiz

/**
 * Question of the quiz that can be checked for correctness.
 */
interface Question {

    /**
     * Gets a flag whether given answer letter is the correct answer (true) or not (false).
     *
     * @return true in case answer is correct, otherwise false
     */
    fun isCorrect(answerLetter: AnswerLetter): Boolean
}