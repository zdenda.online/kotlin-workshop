package online.zdenda.ktw.tasks.quiz

/**
 * Question answer represented by a letter.
 */
enum class AnswerLetter {
    A, B, C, D, E
}