package online.zdenda.ktw.tasks.quiz

/**
 * Factory that is able to build a new pre-defined [Question] instances forming a quiz.
 */
interface QuizFactory {

    /**
     * Creates questions forming the quiz.
     */
    fun newQuestions(): List<Question>
}