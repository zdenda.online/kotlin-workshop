package online.zdenda.ktw.tasks

import online.zdenda.ktw.tasks.quiz.AnswerLetter
import online.zdenda.ktw.tasks.quiz.Question

/**
 * Task #4 is related to advanced functions.
 *
 * Goal is to take input [Question] and be able to print all its correct answer letters WITHOUT:
 * - altering [Question] interface
 * - re-typing to specific implementation of the interface
 * - creating any additional class (e.g. QuestionUtils)
 * - doing the logic inside task class (body of [solve] function must be one simple single function call)
 *
 * Hints:
 * - if not clear right away, it is wanted to create an extension function of [Question]
 * - [AnswerLetter] is an enum and thus have finite values it can have
 * - it is more precise to output [Set] rather than [List]
 */
class Task4 : Task<Question, Set<AnswerLetter>> {

    override fun solve(input: Question): Set<AnswerLetter> {
        TODO("Not yet implemented")
    }
}
