package online.zdenda.ktw.tasks

import online.zdenda.ktw.tasks.quiz.AnswerLetter
import online.zdenda.ktw.tasks.quiz.Question
import online.zdenda.ktw.tasks.quiz.QuizFactory

/**
 * Task #3 is related to simple class implementation with control flow either through `if` or `when` checks.
 *
 * The goal is not to implement only this [Task3.solve] method but also implement a few classes around it as well.
 * There is a [Question] interface for which you should write an implementing class (or multiple classes, up to you).
 * The question implementations will be created within [QuizFactory] implementation that should create two [Question]
 * instances where first question is correct if answer letter is `A` and second question is correct if answer letter
 * is one of (`B`, `C`, `D`).
 *
 * This task accepts list of answers, one for each question (so that list of answers has same size as list of questions)
 * and returns `true` if all provided answers are correct (otherwise `false`).
 *
 * Imagine that calling [QuizFactory.newQuestions] is expensive operation so be sure to call it just once, not on every
 * [solve] call.
 *
 * If the list of answers has different size than list of questions, [IllegalArgumentException] should be thrown with
 * message `Expected <X> answers but got <Y>`.
 *
 * Hints:
 * - when implementing question, it is easier to work with collections, but for "experimenting" try to use `when` clause
 * - on contrary with Java, there is a possibility to [map] along with indices in the single function
 * - there are plenty aggregation-like functions checking predicates on elements of collection
 */
class Task3(
    quizFactory: QuizFactory
) : Task<List<AnswerLetter>, Boolean> {

    override fun solve(input: List<AnswerLetter>): Boolean {
        TODO("Not yet implemented")
    }
}