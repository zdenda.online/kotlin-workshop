package online.zdenda.ktw.tasks

/**
 * Task #5 is free task for which you can choose whatever problem / algorithm to solve,
 *
 * It can be literally anything you come up with now, or anything you solved recently but in different language.
 *
 * In case you don't come up with anything, you can always fall back to classic thing that developers implement when
 * learning a new language and that is a simple task list (e.g. like simple console application). Task list is simple
 * list of text-like things that you should do/buy/make...etc. and each task can be "checked" when finished. I will
 * be around for you to assist you in case you get stuck or if you just want to simply ask anything.
 *
 * Or as a second possibility, you can try play with features of the language on the small examples or study some
 * interesting repositories like
 * - https://github.com/ktorio/ktor-samples
 * - https://github.com/Kotlin/kotlin-examples
 * - https://developer.android.com/courses/kotlin-bootcamp/overview
 * - https://hyperskill.org/knowledge-map/209?track=3
 */
class Task5 : Task<Unit, Unit> {

    override fun solve(input: Unit) {
        TODO("Not yet implemented")
    }
}