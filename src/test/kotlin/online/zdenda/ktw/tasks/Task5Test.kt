package online.zdenda.ktw.tasks

import io.kotest.core.spec.style.BehaviorSpec
import io.kotest.matchers.shouldBe

/**
 * Tests for [Task5].
 */
class Task5Test : BehaviorSpec({

    given("Fifth task and whatever it needs") {
        val task = Task5()

        `when`("solving task") {
            task.solve(Unit)

            then("it does what it should") {
                true shouldBe true
            }
        }
    }
})