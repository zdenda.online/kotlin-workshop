package online.zdenda.ktw.tasks

import io.kotest.assertions.throwables.shouldThrowWithMessage
import io.kotest.core.spec.style.BehaviorSpec
import io.kotest.matchers.shouldBe
import online.zdenda.ktw.tasks.quiz.AnswerLetter
import online.zdenda.ktw.tasks.quiz.AnswerLetter.A
import online.zdenda.ktw.tasks.quiz.AnswerLetter.B
import online.zdenda.ktw.tasks.quiz.AnswerLetter.C
import online.zdenda.ktw.tasks.quiz.Question
import online.zdenda.ktw.tasks.quiz.QuizFactory

/**
 * Tests for [Task3].
 */
class Task3Test : BehaviorSpec({

    given("Third task and your quiz factory") {
        // replace anonymous implementation with your implementation class.
        val quizFactory = object : QuizFactory {
            override fun newQuestions(): List<Question> = TODO("Not yet implemented")
        }
        val task = Task3(quizFactory)

        `when`("solving task with empty list of answers") {
            val answers = listOf<AnswerLetter>()

            then("IllegalArgumentException is thrown") {
                shouldThrowWithMessage<IllegalArgumentException>("Expected 2 answers but got 0") {
                    task.solve(answers)
                }
            }
        }

        `when`("solving task with answers [A, B, C]") {
            val answers = listOf(A, B, C)

            then("IllegalArgumentException is thrown") {
                shouldThrowWithMessage<IllegalArgumentException>("Expected 2 answers but got 3") {
                    task.solve(answers)
                }
            }
        }

        `when`("solving task with answers [A, A]") {
            val output = task.solve(listOf(A, A))

            then("output is false") {
                output shouldBe false
            }
        }

        `when`("solving task with answers [A, B]") {
            val output = task.solve(listOf(A, B))

            then("output is true") {
                output shouldBe true
            }
        }
    }
})