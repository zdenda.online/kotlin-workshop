package online.zdenda.ktw.tasks

import io.kotest.core.spec.style.BehaviorSpec
import io.kotest.matchers.shouldBe
import online.zdenda.ktw.tasks.series.emptySeries
import online.zdenda.ktw.tasks.series.friendsSeries
import online.zdenda.ktw.tasks.series.himymSeries
import online.zdenda.ktw.tasks.series.tbbtSeries

/**
 * Tests for [Task1].
 */
class Task1Test : BehaviorSpec({

    given("First task") {
        val task = Task1()

        `when`("solving it with empty series") {
            val output = task.solve(emptySeries)

            then("output is empty list") {
                output shouldBe emptyList() // this is infix function, we won't go that far in this workshop :-)
            }
        }

        `when`("solving it with Friends series") {
            val output = task.solve(friendsSeries)

            then("output has Joey and Phoebe") {
                output shouldBe listOf("Joey Tribbiani", "Phoebe Buffay")
            }
        }

        `when`("solving it with HIMYM series") {
            val output = task.solve(himymSeries)

            then("output has Barney, Lily, Robin and Ted") {
                output shouldBe listOf("Barney Stinson", "Lily Aldrin", "Robin Scherbatsky", "Ted Mosby")
            }
        }

        `when`("solving it with TBBT series") {
            val output = task.solve(tbbtSeries)

            then("output has just Penny") {
                output shouldBe listOf("Penny")
            }
        }
    }
})