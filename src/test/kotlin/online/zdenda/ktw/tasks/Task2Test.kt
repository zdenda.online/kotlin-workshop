package online.zdenda.ktw.tasks

import io.kotest.core.spec.style.BehaviorSpec
import io.kotest.matchers.shouldBe
import online.zdenda.ktw.tasks.series.commonKnownSeries
import online.zdenda.ktw.tasks.series.emptySeries

/**
 * Tests for [Task2].
 */
class Task2Test : BehaviorSpec({

    given("Second task") {
        val task = Task2()

        `when`("solving task with empty series") {
            val output = task.solve(listOf(emptySeries))

            then("output is empty string") {
                output shouldBe ""
            }
        }

        `when`("solving task with common known series") {
            val output = task.solve(commonKnownSeries)

            then("output is 'comics' as the most frequent hobby of all characters") {
                output shouldBe "comics"
            }
        }
    }
})