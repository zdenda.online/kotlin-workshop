package online.zdenda.ktw.tasks

import io.kotest.core.spec.style.BehaviorSpec
import io.kotest.matchers.collections.shouldContainExactly
import online.zdenda.ktw.tasks.quiz.AnswerLetter.A
import online.zdenda.ktw.tasks.quiz.AnswerLetter.B
import online.zdenda.ktw.tasks.quiz.AnswerLetter.C
import online.zdenda.ktw.tasks.quiz.AnswerLetter.D
import online.zdenda.ktw.tasks.quiz.Question
import online.zdenda.ktw.tasks.quiz.QuizFactory

/**
 * Tests for [Task4].
 */
class Task4Test : BehaviorSpec({

    given("Fourth task and your quiz factory") {
        // replace anonymous implementation with your implementation class.
        val quizFactory = object : QuizFactory {
            override fun newQuestions(): List<Question> = TODO("Not yet implemented")
        }
        val task = Task4()

        `when`("solving task with first question of quiz") {
            val output = task.solve(quizFactory.newQuestions().first())

            then("output is [A]") {
                output shouldContainExactly setOf(A)
            }
        }

        `when`("solving task with last question of quiz") {
            val output = task.solve(quizFactory.newQuestions().last())

            then("output is [B,C,D]") {
                output shouldContainExactly setOf(B, C, D)
            }
        }
    }
})